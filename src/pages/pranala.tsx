import * as React from 'react';
import { NextPage } from 'next';
import styled from '@emotion/styled';

import { Box, Heading, Stack, themeProps } from 'components/design-system';
import { PageWrapper, Content, Column, LinkCard } from 'components/layout';
import importantLinks from 'content/importantLinks.json';
import { SectionHeader } from 'modules/posts-index';

const Section = Content.withComponent('section');

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);

  ${themeProps.mediaQueries.md} {
    grid-column-gap: ${themeProps.space.lg}px;
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 3 - 48px), 1fr)
    );
  }
`;

const ImportantLinkListGrid: React.FC = GridWrapper;

const ImportantLinkPage: NextPage = () => (
  <PageWrapper
    title={`Pranala Penting | KawalCOVID19`}
    description={
      'Halaman ini berisi daftar tautan penting yang berkaitan dengan penanganan COVID-19 di Indonesia'
    }
    pageTitle="Pranala Penting"
  >
    <SectionHeader title={'Pranala Penting Terkait COVID-19'} />
    <Section>
      <Column>
        <Stack spacing="xl">
          {Object.entries(importantLinks).map(([sub, links]) => (
            <Box key={sub}>
              <Heading variant={700} mb="sm" as="h2">
                {sub}
              </Heading>
              <ImportantLinkListGrid>
                {links.map(({ name, link }) => (
                  <LinkCard key={name} name={name} link={link} />
                ))}
              </ImportantLinkListGrid>
            </Box>
          ))}
        </Stack>
      </Column>
    </Section>
  </PageWrapper>
);

export default ImportantLinkPage;
