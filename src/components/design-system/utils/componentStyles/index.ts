import * as typography from './typography';

const componentStyles = {
  typography,
};

export default componentStyles;
